//
//  Networking.swift
//  CarsListMap
//
//  Created by Alex Sklyarenko on 9/21/19.
//  Copyright © 2019 Alex Sklyarenko. All rights reserved.
//

import Foundation

// MARK: - HttpMethods
enum HttpMethods: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

// MARK: - Endpoints
public enum Endpoints {
    case cars

    fileprivate var domain: String { return Configs.API.baseURL }
    fileprivate var scheme: String { return "https" }

    var httpMethod: String! {
        switch self {
        case .cars:
            return HttpMethods.get.rawValue
        }
    }

    var path: String {
        switch self {
        case .cars:
            return "/codingtask/cars"
        }
    }
}

// MARK: - Networking request
public struct Networking {
    static func request<T: Codable>(endpoint: Endpoints,
                                    params: [String: String]?,
                                    completion: @escaping (Result<T, NetError>) -> Void) {

        var urlComponents = URLComponents()
        urlComponents.scheme = endpoint.scheme
        urlComponents.host = endpoint.domain
        urlComponents.path = endpoint.path

        guard let url = urlComponents.url else {
            completion(.failure(.urlEncodeError))
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = endpoint.httpMethod

        if let params = params {
            encodeParamsForRequest(params: params, request: &request)
        }

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error { completion(.failure(.serverError(message: error.localizedDescription))) }
            guard let data = data else {
                completion(.failure(.unknown))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(.unknown))
                return
            }

            // Validate status code
            // TODO: Handle error codes for each case
            guard 200..<300 ~= response.statusCode else {
                return
            }

            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                completion(.success(result))
            } catch let error {
                completion(.failure(.jsonDecodeError(message: error.localizedDescription)))
            }
        }
        task.resume()
    }

    fileprivate static func encodeParamsForRequest(params: [String: String], request: inout URLRequest) {
        switch request.httpMethod {
        case HttpMethods.get.rawValue:
            request.urlEncodeParameters(parameters: params)
        default:
            request.jsonEncodeParams(parameters: params)
        }
    }
}

// MARK: - URLRequest extension for params encoding
fileprivate extension URLRequest {
    mutating func urlEncodeParameters(parameters: [String: String]) {
        guard let url = self.url else { return }
        if let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            var queryParams = [URLQueryItem]()
            for (key, value) in parameters {
                queryParams.append(URLQueryItem(name: key, value: value))
            }
            if !queryParams.isEmpty {
                self.url = urlComponents.url
            }
        }
    }

    mutating func jsonEncodeParams(parameters: [String: String]) {
        self.setValue("application/json", forHTTPHeaderField: "Content-Type")
        httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
    }
}
