//
//  NetError.swift
//  CarsListMap
//
//  Created by Alex Sklyarenko on 9/21/19.
//  Copyright © 2019 Alex Sklyarenko. All rights reserved.
//

import Foundation

enum NetError: Error {
    case serverError(message: String)
    case urlEncodeError
    case jsonDecodeError(message: String)
    case unknown
}
