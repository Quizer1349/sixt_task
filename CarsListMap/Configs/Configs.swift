//
//  Configs.swift
//  CarsListMap
//
//  Created by Alex Sklyarenko on 9/21/19.
//  Copyright © 2019 Alex Sklyarenko. All rights reserved.
//

import Foundation

struct Configs {
    struct API {
        static let baseURL = "cdn.sixt.io"
    }
}
