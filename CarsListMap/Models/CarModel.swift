//
//  CarModel.swift
//  CarsListMap
//
//  Created by Alex Sklyarenko on 9/21/19.
//  Copyright © 2019 Alex Sklyarenko. All rights reserved.
//

import Foundation
import MapKit

enum FuelType: String, Codable {
    case disel = "D"
    case electro = "E"
    case petrol = "P"
}

enum Group: String, Codable {
    case bmw = "BMW"
    case mini = "MINI"
}

enum InnerCleanliness: String, Codable {
    case clean = "CLEAN"
    case regular = "REGULAR"
    case veryClean = "VERY_CLEAN"
}

enum Transmission: String, Codable {
    case auto = "A"
    case manual = "M"
}

struct CarModel: Codable {
    let carId: String
    let modelIdentifier: String
    let modelName: String
    let name: String
    let make: Group
    let group: Group
    let color: String
    let series: String
    let fuelType: FuelType
    let fuelLevel: Double
    let transmission: Transmission
    let licensePlate: String
    let latitude: Double
    let longitude: Double
    let innerCleanliness: InnerCleanliness
    let carImageURL: String

    enum CodingKeys: String, CodingKey {
        case carId = "id"
        case modelIdentifier = "modelIdentifier"
        case modelName = "modelName"
        case name = "name"
        case make = "make"
        case group = "group"
        case color = "color"
        case series = "series"
        case fuelType = "fuelType"
        case fuelLevel = "fuelLevel"
        case transmission = "transmission"
        case licensePlate = "licensePlate"
        case latitude = "latitude"
        case longitude = "longitude"
        case innerCleanliness = "innerCleanliness"
        case carImageURL = "carImageUrl"
    }
}

// MARK: - CoreLocation extension
extension CarModel {
    var coordinates: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var annotation: MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.title = self.name
        annotation.coordinate = self.coordinates
        return annotation
    }
    
}
