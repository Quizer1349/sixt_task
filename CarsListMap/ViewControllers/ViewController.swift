//
//  ViewController.swift
//  CarsListMap
//
//  Created by Alex Sklyarenko on 9/20/19.
//  Copyright © 2019 Alex Sklyarenko. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    // MARK: Properties
    @IBOutlet var mapView: MKMapView!

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        Networking.request(endpoint: .cars, params: nil) { (result: Result<[CarModel], NetError>) in
            switch result {
            case .success(let carList):
                let annotations = carList.map({ (car) -> MKPointAnnotation in
                    return car.annotation
                })
                DispatchQueue.main.async {
                    self.mapView.addAnnotations(annotations)
                    self.mapView.showAnnotations(annotations, animated: true)
                }
            case .failure(let error):
                print(error)
            }
        }
    }

}
